import React from 'react';
import '../style/form.css';

const Form = ({ addToDo, showEditInput, deleteToDo, editToDo, editIsOn, restoreToDo }) => {

    const logicaButton = () => {
        editIsOn ? editToDo() : addToDo();
    }

    return (
        <div className="form">
            <button className="confirm" onClick={() => logicaButton()}>Confirm</button>
            <button className="delete" onClick={() => deleteToDo()}>Delete</button>
            <button className="edit" onClick={() => showEditInput()}>Edit</button>
            <button className="restore" onClick={() => restoreToDo()}>Restore</button>
        </div>
    )
}
export default Form; 