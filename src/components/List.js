import React from 'react';
import '../style/list.css';

const List = ({ showList }) => {

    return (
        <div className="list">
            <div className="inputDiv">
                <input className="input" type="text" placeholder="Add a task..." />
            </div>
            <div className="mainWrapper">
                {showList()}
            </div>
        </div>
    )
}
export default List;