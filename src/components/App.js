import React, { Component } from 'react';
import '../style/app.css';
import Header from './Header';
import Form from './Form';
import List from './List';

class App extends Component {

  state = {
    data: [],
    editIsOn: false,
    editValue: '',
    selectedTask: {
      value: '',
      id: null
    }
  }

  addToDo = () => {
    const input = document.querySelector('.input');
    const inputValue = input.value;
    let todo = {
      task: inputValue,
      isSelected: false
    }
    if (inputValue !== '') {
      let updatedData = [...this.state.data, todo]
      this.setState({ data: updatedData });
      input.value = '';
    } else alert('Add a task first!')

  }

  showList = () => {
    return this.state.data.map((el, index) => {

      const color = el.isSelected === true ? '#217B35' : 'darkred';

      return (
        <div id={index} key={index} className="taskDiv">
          <div>
            {this.appendInputTask(el)}
          </div>
          <div>
            <button style={{ backgroundColor: color }} onClick={() => this.changeIsSelected(index)} className="task-btn"></button>
          </div>
        </div>
      )
    })
  }

  changeIsSelected = (id) => {

    let updatedData = this.state.data.map((el, index) => {
      if (id === index) {
        return {
          task: el.task,
          isSelected: !el.isSelected
        }
      } else {
        return {
          task: el.task,
          isSelected: false
        }
      }
    })
    this.setState({ data: updatedData });
  }

  appendInputTask = (el) => {
    const button = document.querySelector('.edit');

    if (el.isSelected === true && this.state.editIsOn === true) {

      button.style.backgroundColor = 'gold';
      return <input className="editInput" type="text" placeholder="Edit your task..." onChange={(e) => this.handleOnChange(e)} />

    } else {
      button.style.backgroundColor = '#F2B439';
      return <p>{el.task}</p>
    }
  }

  showEditInput = () => {

    this.state.data.map(el => {
      if (el.isSelected === true) {
        this.setState({ editIsOn: !this.state.editIsOn });
      }
    })
  }

  deleteToDo = () => {
    const updatedData = this.state.data.filter(el => el.isSelected === false);
    this.setState({ data: updatedData });
  }

  handleOnChange = (e) => {
    const value = e.target.value;
    this.setState({ editValue: value });
  }

  editToDo = () => {
    const updatedData = this.state.data.map(el => {
      console.log(el.task)

      if (el.isSelected === true) {
        return {
          task: this.state.editValue,
          isSelected: false
        }
      } else if (el.isSelected === false) {
        return {
          task: el.task,
          isSelected: false
        }
      }
    })
    this.setState({ data: updatedData, editIsOn: false, editValue: '' });
  }

  restoreToDo = () => {

    const { data, editIsOn } = this.state

    const singleToDo = data.find(el => el.isSelected);
    const id = data.indexOf(singleToDo);

    const updatedData = data.map((el, index) => {

      if (editIsOn && el.isSelected && index === id) {
        editIsOn = false;
        return {
          task: el.task,
          isSelected: false
        }
      } else {
        return {
          task: el.task,
          isSelected: false
        }
      }
    })
    this.setState({ data: updatedData, editIsOn: false, editValue: '' });
  }

  render() {

    const { addToDo, showEditInput, deleteToDo, editToDo, restoreToDo, showList } = this
    const { editIsOn } = this.state
    return (
      <div className="app">

        <Header />
        <Form addToDo={addToDo}
          showEditInput={showEditInput}
          deleteToDo={deleteToDo}
          editToDo={editToDo}
          editIsOn={editIsOn}
          restoreToDo={restoreToDo} />
        <List showList={showList} />

      </div>
    )
  }
}
export default App;