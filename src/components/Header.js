import React from 'react';
import '../style/header.css';

const Header = () => {

    return (
        <div className="header">
            <h1>ToDo List</h1>
            <h3>React</h3>
        </div>
    )
}

export default Header;